<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $user = DB::table('users')->insert([
            'name' =>'administrator',
            'email' =>'admin@gmail.com',
            'password' => bcrypt('123qwe'),
            'username' =>'admin',
            'phone' =>'123',
            'role' =>'admin',
        ]);

        $menu = DB::table('menus')->insert([
            [
                'name' => 'Home',
                'icon' => 'home',
                'route' => '/',
                'class' => 'header',
                'menu_id' => NULL,
                'count' => 1,
            ],[
                'name' => 'Info',
                'icon' => 'info',
                'route' => '/info',
                'class' => 'header',
                'menu_id' => NULL,
                'count' => 100,
            ],
            [
                'name' => 'Access Management',
                'icon' => 'users',
                'route' => '#',
                'class' => 'header',
                'menu_id' => NULL,
                'count' => 200,
            ],
            [
                'name' => 'Menu',
                'icon' => 'circle',
                'route' => '/menu',
                'class' => 'item',
                'menu_id' => 3,
                'count' => 201,
            ],
            [
                'name' => 'Role',
                'icon' => 'circle',
                'route' => '/role',
                'class' => 'item',
                'menu_id' => 3,
                'count' => 202,
            ]
            ]);

        $role = DB::table('roles')->insert([
            [
                'name' => 'admin'
            ],
            [
                'name' => 'user'
            ]
            ]);

        $roleuser = DB::table('menu_role')->insert([
            [
                'role_id' => 1,
                'menu_id' => 1
            ],
            [
                'role_id' => 1,
                'menu_id' => 2
            ],
            [
                'role_id' => 1,
                'menu_id' => 3
            ],
            [
                'role_id' => 1,
                'menu_id' => 4
            ],
            [
                'role_id' => 1,
                'menu_id' => 5
            ]
            ]);
    }
}
