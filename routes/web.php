<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
    
//     return view('index');
// })->middleware(['auth'])->name('dashboard');


Route::get('logout', [AuthController::class, 'logout'])->name('logout.manual');
Route::get('index', [AuthController::class, 'index'])->name('index');
Route::get('dashboard', [AuthController::class, 'index'])->name('index');

Route::resource('info', App\Http\Controllers\InfoController::class);
Route::resource('menu', App\Http\Controllers\MenuController::class);
Route::get('/menu/{id}/delete', [App\Http\Controllers\MenuController::class, 'destroy'])->name('menu.destroy');


Route::resource('role', App\Http\Controllers\RoleController::class);
Route::get('/role/{id}/delete', [App\Http\Controllers\RoleController::class, 'destroy'])->name('role.destroy');


Route::resource('rolemenu', App\Http\Controllers\MenuRoleController::class);
Route::get('/rolemenu/{id}/delete', [App\Http\Controllers\MenuRoleController::class, 'destroy'])->name('rolemenu.destroy');



require __DIR__.'/auth.php';
