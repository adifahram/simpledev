   <!-- Sidebar -->
   <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      
    </div>

    

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->

             {{-- <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                  Home
                </p>
              </a>
            </li>  --}}
    @php
        $menus = auth()->user()->menusa;
      
    @endphp
   @foreach ($menus as $menu)
   @if ($menu->menu_id === null)
      

       @php
           $subMenus = $menus->where('menu_id', $menu->id);
       @endphp

       @if ($subMenus->isNotEmpty())
           <li class="nav-item">
               <a href="#" class="nav-link">
                   <i class="nav-icon fas fa-{{ $menu->icon }}"></i>
                   <p>
                       {{ $menu->name }}
                       <i class="right fas fa-angle-left"></i>
                   </p>
               </a>
               <ul class="nav nav-treeview">
                   @foreach ($subMenus as $subMenu)
                       <li class="nav-item">
                           <a href="{{ url($subMenu->route) }}" class="nav-link">
                               <i class="far fa-circle nav-icon"></i>
                               <p>{{ $subMenu->name }}</p>
                           </a>
                       </li>
                   @endforeach
               </ul>
           </li>
        @else
        <li class="nav-item">
          <a href="{{ url($menu->route) }}" class="nav-link">
              <i class="nav-icon fas fa-{{ $menu->icon }}"></i>
              <p>{{ $menu->name }}</p>
          </a>
      </li>
       @endif
   @endif
@endforeach

{{-- <li class="nav-item">
            <a href="{{ url('/info') }}" class="nav-link">
              <i class="nav-icon fas fa-info"></i>
              <p>
                Info
              </p>
            </a>
          </li> 
             
        <li class="nav-item menu-open">
          <a href="#" class="nav-link ">
            <i class="nav-icon fas fa-users"></i>
            <p>
                Access Menu
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ url('/menu') }}" class="nav-link ">
                <i class="far fa-circle nav-icon"></i>
                <p>Menu</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/role') }}" class="nav-link ">
                <i class="far fa-circle nav-icon"></i>
                <p>Role</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/rolemenu') }}" class="nav-link ">
                <i class="far fa-circle nav-icon"></i>
                <p>Role Menu</p>
              </a>
            </li>
          </ul>
        </li> --}}

        {{-- <li class="nav-item menu-open">
          <a href="#" class="nav-link active">
            <i class="nav-icon fas fa-users"></i>
            <p>
                User
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link active">
                <i class="far fa-circle nav-icon"></i>
                <p>User Admin</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link ">
                <i class="far fa-circle nav-icon"></i>
                <p>User laundry</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>User Kantin</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>User Wali Murid</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>User Guru</p>
              </a>
            </li>
          </ul>
        </li> --}}
        {{-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
              Simple Link
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li> --}}
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->