<div>

    @isset($datas['multi_data'])

        @foreach ($datas['multi_data'] as $datas => $data)
            <div class="card mt-3 mb-3">
                <div class="">
                        @foreach ($data as $key => $item)
                            @php
                                $i = 0;
                            @endphp

                            @if ($key == 'title')
                                @php
                                    $title = $item;
                                    $snakeTitle = Str::snake($title);
                                @endphp
                                <div class="card-header">

                                    <div class="float-start">
                                        <h5 class="p-1">{{ $title }}</h5>
                                    </div>
                                    <div class="float-end">
                                        <button class="btn btn-primary" type="button"
                                            onClick="add_row('{{ $snakeTitle }}')">
                                            Add Row
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if ($key == 'field')
                                <div class="card-body">
                                <div class="row {{ $snakeTitle }} mb-3" id="{{ $snakeTitle }}_{{ $i }}">
                                    <hr>
                                    <div class="col-md-12">
                                        <div class="row">
                                            @foreach ($item as $fieldName => $field)
                                                <div class="col-md-{{ $field[1]['col'] }}">
                                                    {{-- SELECT TYPE --}}

                                                    @if (!isset($field[1]['select']) && is_array($field[1]['type']))
                                                        <div class="form-group">
                                                            <label
                                                                class="@if (isset($field[1]['required'])) label-required @endif"
                                                                for="{{ $fieldName }}">{{ $field[0] }}
                                                            </label>

                                                            <select
                                                                name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                class="form-control"
                                                                @if (isset($field[1]['id'])) id="{{ $field[1]['id'] }}" @endif
                                                                @if (isset($field[1]['js'])) {{ $field[1]['js'] }}="{{ $field[1]['function'] }}(this.value,'{{ $field[1]['target'] }}')" @endif
                                                                @if (isset($field[1]['required'])) required @endif>
                                                                <option value="">Select</option>
                                                                @foreach ($field[1]['type'] as $value => $label)
                                                                    <option
                                                                        @if (
                                                                            (isset($datas['data']) && isset($datas['data'][$fieldName]) && $datas['data'][$fieldName] == $value) ||
                                                                                old($fieldName) == $value ||
                                                                                (isset($field[1]['names']) && isset($datas['data']) && isset($datas['data'][$field[1]['names']])
                                                                                    ? $datas['data'][$field[1]['names']] == $value
                                                                                    : old($fieldName))) selected @endif
                                                                        value="{{ $value }}">
                                                                        {{ $label }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @elseif (isset($field[1]['select']) && $field[1]['select'] == 'select2')
                                                        <div class="form-group">
                                                            <label
                                                                class="@if (isset($field[1]['required'])) label-required @endif"
                                                                for="{{ $fieldName }}">{{ $field[0] }}
                                                            </label>

                                                            <select
                                                                name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                class="form-control select2-single" style="width: 100%"
                                                                @if (isset($field[1]['required'])) required @endif>
                                                                <option value="">Select</option>
                                                                @foreach ($field[1]['type'] as $value => $label)
                                                                    <option value="{{ $value }}">
                                                                        {{ $label }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    @elseif ($field[1]['type'] == 'detail')
                                                        {{-- INPUTAN DETAIL  --}}
                                                        <br>
                                                        <br>
                                                        <h3>{{ $field[0] }}</h3>
                                                        <x-form-input-detail :datas="$field[1]" :id="$field[1]['id']"
                                                            :details="isset($datas['details'])
                                                                ? $datas['details']
                                                                : null" />


                                                        {{-- INPUTAN HIDDEN --}}
                                                    @elseif ($field[1]['type'] == 'hidden')
                                                        <div class="form-group">
                                                            <input @if (isset($field[1]['required'])) required @endif
                                                                type="{{ $field[1]['type'] }}"
                                                                value="{{ isset($datas['data']) ? $datas['data'][$fieldName] : old($fieldName) }}"
                                                                name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                placeholder="{{ $field[0] }}..." class="form-control">
                                                        </div>


                                                        {{-- INPUTAN Password --}}
                                                    @elseif ($field[1]['type'] == 'password')
                                                        <div class="form-group">
                                                            <label
                                                                class="@if (isset($field[1]['required'])) label-required @endif"
                                                                for="{{ $fieldName }}">{{ $field[0] }}
                                                            </label>
                                                            <input @if (isset($field[1]['required'])) required @endif
                                                                type="{{ $field[1]['type'] }}" value=""
                                                                placeholder="{{ $field[0] }}..." class="form-control"
                                                                name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]">
                                                        </div>



                                                        {{-- INPUTAN BIASA --}}
                                                    @else
                                                        <div class="form-group">
                                                            <label
                                                                class="@if (isset($field[1]['required'])) label-required @endif"
                                                                for="{{ $fieldName }}">{{ $field[0] }}
                                                            </label>
                                                            <br>
                                                            @if (isset($datas['data']) && $field[1]['type'] == 'file' && isset($datas['data'][$fieldName]))
                                                                <img src="{{ url($datas['data'][$fieldName]) }}"
                                                                    alt="" width="50px">
                                                                <br>
                                                                <br>
                                                            @endif

                                                            <input @if (isset($field[1]['required'])) required @endif
                                                                type="{{ $field[1]['type'] }}"
                                                                value="{{ isset($field[1]['names']) && isset($datas['data']) && isset($datas['data'][$field[1]['names']]) ? $datas['data'][$field[1]['names']] : (isset($datas['data']) ? (isset($datas['data'][$fieldName]) ? $datas['data'][$fieldName] : old($fieldName)) : old($fieldName)) }}"
                                                                name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                placeholder="{{ $field[0] }}..." class="form-control">
                                                        </div>
                                                    @endif

                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-1 ml-auto d-flex align-items-center justify-content-end">
                                        <div class="row">
                                            <button class="btn btn-danger" type="button"
                                                onclick="remove_row(this, '{{ $snakeTitle }}');">
                                                Remove
                                            </button>
                                        </div>
                                    </div>
                                    @php
                                        $i++;
                                    @endphp

                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @endisset

    @isset($datas['multi_data_edit'])
        @foreach ($datas['multi_data_edit'] as $data)
            <div class="card ">
                <div class="">
                        @foreach ($data as $key => $item)
                            @php
                                $i = 0;
                            @endphp

                            @if ($key == 'title')
                                @php
                                    $title = $item;
                                    $snakeTitle = Str::snake($title);
                                @endphp
                                <div class="card-header">
                                    <div class="">
                                        <h5 class="p-1">{{ $title }}</h5>
                                    </div>
                                    <div class="">
                                        <button class="btn btn-primary" type="button"
                                            onClick="add_row('{{ $snakeTitle }}')">
                                            Add Row
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if ($key == 'field')   
                                @if (isset($datas['data'][$snakeTitle][0]))
                                    @foreach ($datas['data'][$snakeTitle]->toArray() as $relation_name => $relation_value)
                                        @php
                                        @endphp
                                <div class="card-body">
                                    <div class="row {{ $snakeTitle }} mb-3"
                                            id="{{ $snakeTitle }}_{{ $i }}">
                                            <hr>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    @foreach ($item as $fieldName => $field)
                                                        <div class="col-md-{{ $field[1]['col'] }}">
                                                            {{-- SELECT TYPE --}}

                                                            @if (!isset($field[1]['select']) && is_array($field[1]['type']))
                                                                <div class="form-group">
                                                                    <label
                                                                        class="@if (isset($field[1]['required'])) label-required @endif"
                                                                        for="{{ $fieldName }}">{{ $field[0] }}
                                                                    </label>

                                                                    <select
                                                                        name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                        class="form-control"
                                                                        @if (isset($field[1]['id'])) id="{{ $field[1]['id'] }}{{ $i }}" @endif
                                                                        @if (isset($field[1]['js'])) {{ $field[1]['js'] }}="{{ $field[1]['function'] }}(this.value,'{{ $field[1]['target'] }}','{{ $i }}')" @endif
                                                                        @if (isset($field[1]['required'])) required @endif>
                                                                        <option value="">Select</option>
                                                                        @foreach ($field[1]['type'] as $value => $label)
                                                                            <option
                                                                                @if (old($fieldName) == $value ||
                                                                                        ((isset($snakeTitle) &&
                                                                                            isset($fieldName) &&
                                                                                            isset($snakeTitle[$fieldName]) &&
                                                                                            old($snakeTitle[$fieldName]) == $value) ||
                                                                                            (isset($snakeTitle) &&
                                                                                                isset($field[1]['names']) &&
                                                                                                isset($snakeTitle[$field[1]['names']]) &&
                                                                                                old($snakeTitle[$field[1]['names']]) == $value) ||
                                                                                            (isset($relation_value) && isset($relation_value[$fieldName]) && $relation_value[$fieldName] == $value) ||
                                                                                            (isset($field[1]['names']) &&
                                                                                                isset($relation_value) &&
                                                                                                isset($relation_value[$field[1]['names']]) &&
                                                                                                $relation_value[$field[1]['names']] == $value))) selected @endif
                                                                                value="{{ $value }}">
                                                                                {{ $label }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            @elseif (isset($field[1]['select']) && $field[1]['select'] == 'select2')
                                                            <div class="form-group">
                                                                <label
                                                                    class="@if (isset($field[1]['required'])) label-required @endif"
                                                                    for="{{ $fieldName }}">{{ $field[0] }}
                                                                </label>
                                                                <select
                                                                    name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                    class="form-control select2-single" style="width: 100%"
                                                                    @if (isset($field[1]['id'])) id="{{ $field[1]['id'] }}{{ $i }}" @endif
                                                                    @if (isset($field[1]['js'])) {{ $field[1]['js'] }}="{{ $field[1]['function'] }}(this.value,'{{ $field[1]['target'] }}','{{ $i }}')" @endif
                                                                    @if (isset($field[1]['required'])) required @endif>
                                                                    <option value="">Select</option>
                                                                    @foreach ($field[1]['type'] as $value => $label)
                                                                        <option
                                                                            @if (old($fieldName) == $value ||
                                                                                    ((isset($snakeTitle) &&
                                                                                        isset($fieldName) &&
                                                                                        isset($snakeTitle[$fieldName]) &&
                                                                                        old($snakeTitle[$fieldName]) == $value) ||
                                                                                        (isset($snakeTitle) &&
                                                                                            isset($field[1]['names']) &&
                                                                                            isset($snakeTitle[$field[1]['names']]) &&
                                                                                            old($snakeTitle[$field[1]['names']]) == $value) ||
                                                                                        (isset($relation_value) && isset($relation_value[$fieldName]) && $relation_value[$fieldName] == $value) ||
                                                                                        (isset($field[1]['names']) &&
                                                                                            isset($relation_value) &&
                                                                                            isset($relation_value[$field[1]['names']]) &&
                                                                                            $relation_value[$field[1]['names']] == $value))) selected @endif
                                                                            value="{{ $value }}">
                                                                            {{ $label }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                                {{-- INPUTAN DETAIL  --}}
                                                            @elseif ($field[1]['type'] == 'detail')
                                                                <br>
                                                                <br>
                                                                <h3>{{ $field[0] }}</h3>
                                                                <x-form-input-detail :datas="$field[1]" :id="$field[1]['id']"
                                                                    :details="isset($datas['details'])
                                                                        ? $datas['details']
                                                                        : null" />


                                                                {{-- INPUTAN HIDDEN --}}
                                                            @elseif ($field[1]['type'] == 'hidden')
                                                                <div class="form-group">
                                                                    <input
                                                                        @if (isset($field[1]['required'])) required @endif
                                                                        type="{{ $field[1]['type'] }}"
                                                                        value="{{ isset($relation_value) ? $relation_value[$fieldName] : old($fieldName) }}"
                                                                        name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                        placeholder="{{ $field[0] }}..."
                                                                        class="form-control">
                                                                </div>


                                                                {{-- INPUTAN Password --}}
                                                            @elseif ($field[1]['type'] == 'password')
                                                                <div class="form-group">
                                                                    <label
                                                                        class="@if (isset($field[1]['required'])) label-required @endif"
                                                                        for="{{ $fieldName }}">{{ $field[0] }}
                                                                    </label>
                                                                    <input
                                                                        @if (isset($field[1]['required'])) required @endif
                                                                        type="{{ $field[1]['type'] }}" value=""
                                                                        placeholder="{{ $field[0] }}..."
                                                                        class="form-control"
                                                                        name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]">
                                                                </div>



                                                                {{-- INPUTAN BIASA --}}
                                                            @else
                                                                <div class="form-group">
                                                                    <label
                                                                        class="@if (isset($field[1]['required'])) label-required @endif"
                                                                        for="{{ $fieldName }}">{{ $field[0] }}
                                                                    </label>
                                                                    <br>
                                                                    @if (isset($relation_value) && $field[1]['type'] == 'file' && isset($relation_value[$fieldName]))
                                                                        <img src="{{ url($relation_value[$fieldName]) }}"
                                                                            alt="" width="50px">
                                                                        <br>
                                                                        <br>
                                                                    @endif

                                                                    <input
                                                                        @if (isset($field[1]['required'])) required @endif
                                                                        type="{{ $field[1]['type'] }}"
                                                                        value="{{ isset($field[1]['names']) && isset($relation_value) && isset($relation_value[$field[1]['names']]) ? $relation_value[$field[1]['names']] : (isset($relation_value) ? (isset($relation_value[$fieldName]) ? $relation_value[$fieldName] : old($fieldName)) : old($fieldName)) }}"
                                                                        name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                                        placeholder="{{ $field[0] }}..."
                                                                        class="form-control">
                                                                </div>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="col-md-1 ml-auto d-flex align-items-center justify-content-end">
                                                <div class="row">
                                                    <button class="btn btn-danger" type="button"
                                                        onclick="remove_row(this, '{{ $snakeTitle }}');">
                                                        Remove
                                                    </button>
                                                </div>
                                            </div>
                                            @php
                                                $i++;
                                            @endphp
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                
                                {{-- ADD New row in edit --}}

                                <div class="row {{ $snakeTitle }} mb-3"
                                id="{{ $snakeTitle }}_{{ $i }}">
                                <hr>
                                <div class="card-body">

                                <div class="col-md-12">
                                    <div class="row">
                                        @foreach ($item as $fieldName => $field)
                                            <div class="col-md-{{ $field[1]['col'] }}">
                                                {{-- SELECT TYPE --}}

                                                @if (!isset($field[1]['select']) && is_array($field[1]['type']))
                                                    <div class="form-group">
                                                        <label
                                                            class="@if (isset($field[1]['required'])) label-required @endif"
                                                            for="{{ $fieldName }}">{{ $field[0] }}
                                                        </label>

                                                        <select
                                                            name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                            class="form-control"
                                                            @if (isset($field[1]['id'])) id="{{ $field[1]['id'] }}" @endif
                                                            @if (isset($field[1]['js'])) {{ $field[1]['js'] }}="{{ $field[1]['function'] }}(this.value,'{{ $field[1]['target'] }}')" @endif
                                                            @if (isset($field[1]['required'])) required @endif>
                                                            <option value="">Select</option>
                                                            @foreach ($field[1]['type'] as $value => $label)
                                                                <option
                                                                    @if (
                                                                        (isset($datas['data']) && isset($datas['data'][$fieldName]) && $datas['data'][$fieldName] == $value) ||
                                                                            old($fieldName) == $value ||
                                                                            (isset($field[1]['names']) && isset($datas['data']) && isset($datas['data'][$field[1]['names']])
                                                                                ? $datas['data'][$field[1]['names']] == $value
                                                                                : old($fieldName))) selected @endif
                                                                    value="{{ $value }}">
                                                                    {{ $label }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @elseif (isset($field[1]['select']) && $field[1]['select'] == 'select2')
                                                    <div class="form-group">
                                                        <label
                                                            class="@if (isset($field[1]['required'])) label-required @endif"
                                                            for="{{ $fieldName }}">{{ $field[0] }}
                                                        </label>

                                                        <select
                                                            name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                            class="form-control select2-single" style="width: 100%"
                                                            @if (isset($field[1]['required'])) required @endif>
                                                            <option value="">Select</option>
                                                            @foreach ($field[1]['type'] as $value => $label)
                                                                <option value="{{ $value }}">
                                                                    {{ $label }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                @elseif ($field[1]['type'] == 'detail')
                                                    {{-- INPUTAN DETAIL  --}}
                                                    <br>
                                                    <br>
                                                    <h3>{{ $field[0] }}</h3>
                                                    <x-form-input-detail :datas="$field[1]" :id="$field[1]['id']"
                                                        :details="isset($datas['details'])
                                                            ? $datas['details']
                                                            : null" />


                                                    {{-- INPUTAN HIDDEN --}}
                                                @elseif ($field[1]['type'] == 'hidden')
                                                    <div class="form-group">
                                                        <input @if (isset($field[1]['required'])) required @endif
                                                            type="{{ $field[1]['type'] }}"
                                                            value="{{ isset($datas['data']) ? $datas['data'][$fieldName] : old($fieldName) }}"
                                                            name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                            placeholder="{{ $field[0] }}..." class="form-control">
                                                    </div>


                                                    {{-- INPUTAN Password --}}
                                                @elseif ($field[1]['type'] == 'password')
                                                    <div class="form-group">
                                                        <label
                                                            class="@if (isset($field[1]['required'])) label-required @endif"
                                                            for="{{ $fieldName }}">{{ $field[0] }}
                                                        </label>
                                                        <input @if (isset($field[1]['required'])) required @endif
                                                            type="{{ $field[1]['type'] }}" value=""
                                                            placeholder="{{ $field[0] }}..." class="form-control"
                                                            name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]">
                                                    </div>



                                                    {{-- INPUTAN BIASA --}}
                                                @else
                                                    <div class="form-group">
                                                        <label
                                                            class="@if (isset($field[1]['required'])) label-required @endif"
                                                            for="{{ $fieldName }}">{{ $field[0] }}
                                                        </label>
                                                        <br>
                                                        @if (isset($datas['data']) && $field[1]['type'] == 'file' && isset($datas['data'][$fieldName]))
                                                            <img src="{{ url($datas['data'][$fieldName]) }}"
                                                                alt="" width="50px">
                                                            <br>
                                                            <br>
                                                        @endif

                                                        <input @if (isset($field[1]['required'])) required @endif
                                                            type="{{ $field[1]['type'] }}"
                                                            value="{{ isset($field[1]['names']) && isset($datas['data']) && isset($datas['data'][$field[1]['names']]) ? $datas['data'][$field[1]['names']] : (isset($datas['data']) ? (isset($datas['data'][$fieldName]) ? $datas['data'][$fieldName] : old($fieldName)) : old($fieldName)) }}"
                                                            name="{{ $snakeTitle }}[{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}][]"
                                                            placeholder="{{ $field[0] }}..." class="form-control">
                                                    </div>
                                                @endif

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-1 ml-auto d-flex align-items-center justify-content-end">
                                    <div class="row">
                                        <button class="btn btn-danger" type="button"
                                            onclick="remove_row(this, '{{ $snakeTitle }}');">
                                            Remove
                                        </button>
                                    </div>
                                </div>
                                @php
                                    $i++;
                                @endphp
                                </div>

                                {{-- end --}}
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @endisset
</div>

@push('script')
    <script>
        function add_row(item) {
            const target = document.getElementsByClassName(`${item}`);
            var clonedTarget = target[target.length - 1].cloneNode(true);
            let inputTag = clonedTarget.getElementsByTagName('input');
            let selectTag = clonedTarget.getElementsByTagName('select');

            for (let i = 0; i < inputTag.length; ++i) {
                if (clonedTarget.getElementsByTagName('input')[i].value != '')
                    clonedTarget.getElementsByTagName('input')[i].value = "";
            }
            for (let i = 0; i < selectTag.length; ++i) {
                if (clonedTarget.getElementsByTagName('select')[i].value != '')
                    clonedTarget.getElementsByTagName('select')[i].value = "";
            }
           // Menghapus elemen gambar dari klon
            const imgTags = clonedTarget.getElementsByTagName('img');
            for (let i = 0; i < imgTags.length; i++) {
                imgTags[i].parentNode.removeChild(imgTags[i]);
            }
            target[0].parentNode.insertBefore(clonedTarget, target[0]);
        };

        function remove_row(e, item) {
            const buttons = document.querySelectorAll('button');
            buttons.forEach((button) => {
                button.setAttribute('disabled', true)
            })
            const target = document.getElementsByClassName(`${item}`);
            if (target.length > 1) {
                const rowId = e.parentNode.parentNode.parentNode.querySelectorAll('input[type=hidden]');
                if (rowId.length == 0 || rowId[0].getAttribute("value") == '') {
                    e.parentNode.parentNode.parentNode.remove();
                    Swal.fire({
                        icon: "success",
                        title: "Row Deleted!"
                    });
                    buttons.forEach((button) => {
                        button.removeAttribute('disabled');
                    })
                } else {
                    let url = `${item}/delete?id=${rowId[0].getAttribute("value")}`
                    $.ajax({
                        url: `${url}`,
                        method: 'GET',
                        success: function(result) {
                            buttons.forEach((button) => {
                                button.removeAttribute('disabled');
                            })
                            Swal.fire({
                                icon: "success",
                                title: "Row Deleted"
                            });
                            e.parentNode.parentNode.parentNode.remove();
                        },
                        error: function(result) {
                            buttons.forEach((button) => {
                                button.removeAttribute('disabled');
                            })
                            Swal.fire({
                                icon: "error",
                                title: "Failed Delete Row"
                            });
                        }
                    });
                }
            } else {
                buttons.forEach((button) => {
                    button.removeAttribute('disabled');
                })
                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: `Can't Delete Last Row`,
                    background: "#0e1726",
                    color: "white",
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        }





        function on_change(e, item, ids = null) {
            const target = document.getElementsByClassName(`${item}`);
            console.log(`${item}?id=${e}`);
            let url = `${item}?id=${e}`
            $.ajax({
                url: `${url}`,
                method: 'GET',
                success: function(result) {
                    if (ids) {
                        $("#" + item + ids).empty();
                        for (const key in result) {
                            if (result.hasOwnProperty(key)) {
                                $("#" + item + ids).append(`<option value="${key}">${result[key]}</option>`);
                            }
                        }

                    } else {
                        $("#" + item).empty();
                        for (const key in result) {
                            if (result.hasOwnProperty(key)) {
                                $("#" + item).append(`<option value="${key}">${result[key]}</option>`);
                            }
                        }

                    }


                    Toast.fire({
                        icon: "success",
                        title: "Change Select"
                    });
                },
                error: function(result) {
                    Toast.fire({
                        icon: "error",
                        title: "Failed Change Select"
                    });
                }
            });
        }
    </script>
@endpush
<style>
    .select2-selection__rendered {
        line-height: 46px !important;
    }

    .select2-container .select2-selection--single {
        height: 50px !important;
    }

    .select2-selection__arrow {
        height: 49px !important;
    }
</style>