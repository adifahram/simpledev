
<div>
    <br> 
    <div class="row">
        <br>
        <table class="table" id="{{$id}}">
            @if (isset($details))
            @php
                $no = 2000;
            @endphp
                @foreach ($details as $kys => $item)
                <tr>

                    @foreach ($datas['datas'] as $fieldName => $field)
                    <td>
                        @if (is_array($field[1]['type'])) 
                        @php
                            $no++;
                        @endphp
                            <div class="form-group">
                                <label for="{{ $fieldName }}">{{ $field[0] }} </label>
                                <select
                                @if (isset($field[1]['required']))
                                    required
                                @endif
                                name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]"
                                id="{{ $fieldName.$no }}"
                                class="form-control">
                                <option value="" >Select</option>
                                    @foreach ($field[1]['type'] as $value => $label)
                                  
                                        <option
                                        @if ($item && isset($item[$fieldName]) && $item[$fieldName] == $value || old($fieldName) == $value || isset($field[1]['names']) && isset($item) && $item[$field[1]['names']] == $value)
                                        selected
                                    @endif
                                        value="{{ $value }}">{{ $label }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- @push('script')
                            <script>
                                $( document ).ready(function() {
                                    new TomSelect("#{{ $fieldName.$no }}",{
                                        maxItems: 1
                                });
                                });
                               
                            </script>
                            @endpush  --}}


                        @elseif ($field[1]['type'] == 'button')
                            {{-- @php
                                dd($field[1])
                            @endphp --}}
                                <br>
                            <button type='button' onclick='deleteRow(this)' class='btn btn-sm btn-danger'> Delete </button>
                         
                        @else
                            <div class="form-group">
                                <label for="{{ $fieldName }}">{{ $field[0] }} </label>

                                @if (isset($item ) && $field[1]['type'] == 'file')
                                    <img src="{{url($item[$fieldName])}}" alt="" width="50px">
                                @endif
                                <input
                                @if (isset($field[1]['required']))
                                    required
                                @endif 
                                type="{{ $field[1]['type'] }}"
                                value="{{ $item ? $item[$fieldName] : old($fieldName) }}"
                                name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]"
                                id="{{ $fieldName }}"
                                placeholder="{{ $field[0] }}..."
                                class="form-control">
                            </div>
                        @endif
                    </td>
    
                    
                   
                    @endforeach
                    {{-- <x-js-input :datas="$datas['datas']"> --}}
                </tr>
                @endforeach
            @endif
            <tr>
                
                @php
                $nos = 1000;
            @endphp

                @foreach ($datas['datas'] as $fieldName => $field)
                @php
                    $nos++;
                @endphp
                <td>
                    @if (is_array($field[1]['type']))
                        <div class="form-group">
                            <label for="{{ $fieldName }}">{{ $field[0] }} </label>
                            <select
                            name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]" 
                            id="{{ $fieldName.$nos }}"
                            class="form-control">
                                <option value="" selected>Select</option>
                                @foreach ($field[1]['type'] as $value => $label)
                                    <option value="{{ $value }}">{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- @push('script')
                            <script>
                                $( document ).ready(function() {
                                    new TomSelect("#{{ $fieldName.$nos }}",{
                                        maxItems: 1
                                });
                                });
                               
                            </script>
                            @endpush  --}}
                    @elseif ($field[1]['type'] == 'button')
                        {{-- @php
                            dd($field[1])
                        @endphp --}}
                            <br>
                        <button type="button" class="btn btn-primary" @if ($field[1]['js'])
                            {{$field[1]['js']}}="{{$field[1]['function']}}{{$id}}()"
                        @endif >Add</button>
                     
                    @else
                        <div class="form-group">
                            <label for="{{ $fieldName }}">{{ $field[0] }} </label>
                            
                            <input type="{{ $field[1]['type'] }}" 
                            name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]" 
                            id="{{ $fieldName }}"
                            placeholder="{{ $field[0] }}..." 
                            class="form-control">
                        </div>
                    @endif
                </td>

                
               
                @endforeach
                {{-- <x-js-input :datas="$datas['datas']"> --}}
            </tr>
            
        </table>
       
    </div>
</div>

@push('script')
<script>
    var lin = 0;
    function add_row{{$id}}() {
        // alert('tessss')
        lin = lin +1;
        var newRow = `<tr>
            
            @php
                $nof = '';
            @endphp
            @foreach ($datas['datas'] as $fieldName => $field)
            <td>
                @php
                $nof = '`+lin+`';
            @endphp
                    @if (is_array($field[1]['type']))
                        <div class="form-group">
                            <select 
                            name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]" 
                            id="{{ $fieldName. $nof }}"
                            class="form-control">
                                <option value="" selected>Select</option>
                                @foreach ($field[1]['type'] as $value => $label)
                                    <option value="{{ $value }}">{{ $label }}</option>
                                @endforeach
                            </select>
                        </div>
                        @push('script')
                            <script>
                                $( document ).ready(function() {
                                    new TomSelect("#{{ $fieldName. $nof }}",{
                                        maxItems: 1
                                });
                                });
                               
                            </script>
                            @endpush 
                    @elseif ($field[1]['type'] == 'button')
                    <button type='button' onclick='deleteRow(this)' class='btn btn-sm btn-danger'> Delete </button>
                     
                    @else
                        <div class="form-group">
                            <input
                            type="{{ $field[1]['type'] }}"
                            name="{{ isset($field[1]['names']) ? $id.$field[1]['names'] :  $id.$fieldName }}[]"
                            id="{{ $fieldName }}"
                            placeholder="{{ $field[0] }}..."
                            class="form-control">
                        </div>
                    @endif
                </td>

                
               
                @endforeach
                </tr>`;
    $('#{{$id}} tbody').append(newRow);
    }

    function deleteRow(btn) {
        var row = btn.parentNode.parentNode;
        row.parentNode.removeChild(row);
    }
                     
</script>
@endpush
