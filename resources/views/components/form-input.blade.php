<div>
    {{-- @php
        dd($datas['details'])
    @endphp --}}
    <form action="{{ route($datas['route']) }}" method="{{ $datas['method'] }}" enctype="multipart/form-data">

        <div class="card mt-3 mb-3">
            <div class="card-body">
                @csrf
                @method($datas['method'])
                <div class="row">

                    @foreach ($datas['datas'] as $fieldName => $field)
                        <div class="col-md-{{ $field[1]['col'] }}">
                            {{-- SELECT TYPE --}}
                            @if (is_array($field[1]['type']))
                                <div class="form-group">
                                    <label for="{{ $fieldName }}"
                                        class="fw-bold @if (isset($field[1]['required'])) label-required @endif">{{ $field[0] }}
                                    </label>

                                    <select @if (isset($field[1]['select2'])) multiple @endif
                                        name="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}{{ isset($field[1]['select2']) ? '[]' : '' }}"
                                        id="{{ $fieldName }}" class="form-control select2"
                                        style="@if (isset($field[1]['select2'])) height:10px @endif"
                                        @if (isset($field[1]['required'])) required @endif>
                                        <option value="">Select </option>
                                        @php
                                            $savedValues = explode('|', old($fieldName, isset($field[1]['names']) && isset($datas['data']) ? $datas['data'][$field[1]['names']] : $datas['data'][$fieldName] ?? ''));
                                        @endphp

                                        @foreach ($field[1]['type'] as $value => $label)
                                            @php
                                                $selected = false;
                                                foreach ($savedValues as $savedValuesvalue) {
                                                    if ($savedValuesvalue == $value) {
                                                        $selected = true;
                                                        break; // Exit the loop once a match is found
                                                    }
                                                }
                                            @endphp
                                            <option @if (old($fieldName) == $value ||
                                                    (isset($field[1]['names']) && old($field[1]['names']) == $value) ||
                                                    (isset($field[1]['names']) &&
                                                        isset($datas['data']) &&
                                                        isset($datas['data'][$field[1]['names']]) &&
                                                        $datas['data'][$field[1]['names']] == $value) ||
                                                    ((isset($datas['data']) && isset($datas['data'][$fieldName]) && $datas['data'][$fieldName] == $value) ||
                                                        $selected)) selected @endif
                                                value="{{ $value }}">{{ $label }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- @push('script')
                                    <script>
                                        $(document).ready(function() {
                                            new TomSelect("#{{ $fieldName }}", {
                                                maxItems: {{ isset($field[1]['select2']) ? 100 : 1 }}
                                            });
                                        });
                                    </script>
                                @endpush --}}

                                {{-- INPUTAN DETAIL  --}}
                            @elseif ($field[1]['type'] == 'detail')
                                <br>
                                <br>
                        </div>
                </div>
            </div>
        </div>
        <div class="card mt-3 mb-3">
            <div class="card-body">
                <div class="row">
                    <h3>{{ $field[0] }}</h3>
                    <x-form-input-detail :datas="$field[1]" :id="$field[1]['id']" :details="isset($datas['details']) ? $datas['details'] : null" />


                    {{-- INPUTAN HIDDEN --}}
                @elseif ($field[1]['type'] == 'hidden')
                    <div class="form-group">
                        <input @if (isset($field[1]['required'])) required @endif
                            @if (isset($field[1]['disabled'])) disabled @endif type="{{ $field[1]['type'] }}"
                            value="{{ isset($datas['data']) ? $datas['data'][$fieldName] : old($fieldName) }}"
                            name="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}"
                            id="{{ $fieldName }}" placeholder="{{ $field[0] }}..." class="form-control">
                    </div>

                    {{-- INPUTAN Password --}}
                @elseif ($field[1]['type'] == 'password')
                    <div class="form-group">
                        <label for="{{ $fieldName }}"
                            class="fw-bold @if (isset($field[1]['required'])) label-required @endif">{{ $field[0] }}
                        </label>
                        <input @if (isset($field[1]['required'])) required @endif type="{{ $field[1]['type'] }}"
                            value="" id="{{ $fieldName }}" placeholder="{{ $field[0] }}..."
                            class="form-control"
                            name="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}">
                    </div>
                    
                    {{-- INPUTAN texarea --}}
                @elseif ($field[1]['type'] == 'textarea')
                <div class="form-group">
                    <label for="{{ $fieldName }}"
                        class="fw-bold @if (isset($field[1]['required'])) label-required @endif">{{ $field[0] }}
                    </label>
                    <textarea
                    class="form-control"
                     @if (isset($field[1]['required'])) required @endif
                    name="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}"
                    id="{{ $fieldName }}" cols="30" rows="10">{{ old($fieldName) ?? (isset($field[1]['names']) && old($field[1]['names']) != null ? old($field[1]['names']) : (isset($datas['data']) && isset($field[1]['names']) && isset($datas['data'][$field[1]['names']]) ? $datas['data'][$field[1]['names']] : (isset($datas['data']) && isset($datas['data'][$fieldName]) ? $datas['data'][$fieldName] : ''))) }}</textarea>
                  
                </div>
                @elseif ($field[1]['type'] == 'table')
                    <x-form-table :datas="$field" />
                @elseif ($field[1]['type'] == 'datetimepicker')
                    @php
                        if (isset($field[1]['required'])) {
                            $isRequired = 'true';
                        } else {
                            $isRequired = 'false';
                        }
                    @endphp
                    <x-datepicker isRequired="{{ $isRequired }}"
                        value="{{ old($fieldName) ?? (isset($field[1]['names']) && old($field[1]['names']) != null ? old($field[1]['names']) : (isset($datas['data']) && isset($field[1]['names']) && isset($datas['data'][$field[1]['names']]) ? $datas['data'][$field[1]['names']] : (isset($datas['data']) && isset($datas['data'][$fieldName]) ? $datas['data'][$fieldName] : ''))) }}"
                        inputId="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}"
                        elementId="datetime{{ $fieldName }}" title="{{ $field[0] }}" />
                @else
                    <div class="form-group">
                        <label for="{{ $fieldName }}"
                            class="fw-bold @if (isset($field[1]['required'])) label-required @endif">{{ $field[0] }}
                        </label>
                        <br>
                        @if (isset($datas['data']) && $field[1]['type'] == 'file' && isset($datas['data'][$fieldName]))
                            <img src="{{ url($datas['data'][$fieldName]) }}" alt="" width="50px">
                            <br>
                            <br>
                        @endif
                        <input @if (isset($field[1]['required'])) required @endif
                            @if (isset($field[1]['disabled'])) disabled @endif type="{{ $field[1]['type'] }}"
                            value="{{ old($fieldName) ?? (isset($field[1]['names']) && old($field[1]['names']) != null ? old($field[1]['names']) : (isset($datas['data']) && isset($field[1]['names']) && isset($datas['data'][$field[1]['names']]) ? $datas['data'][$field[1]['names']] : (isset($datas['data']) && isset($datas['data'][$fieldName]) ? $datas['data'][$fieldName] : ''))) }}"
                            name="{{ isset($field[1]['names']) ? $field[1]['names'] : $fieldName }}"
                            id="{{ $fieldName }}" placeholder="{{ $field[0] }}..." class="form-control">
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
</div>

@if (isset($datas['multi_data']) || isset($datas['multi_data_edit']))
    <x-form-input-multi-row :datas="$datas" />
@endif

@if (!isset($datas['hidden_button']))
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
@endif
</div>
</form>
</div>
