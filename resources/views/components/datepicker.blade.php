<div class="col-sm-12" id="htmlTarget">
    @if (isset($title))
        <label @isset($labelId)
        id="{{ $labelId }}"
        @endisset
            class="@if (isset($isRequired) && $isRequired == 'true') label-required @endif fw-bold"
            for="{{ $inputId }}">{{ $title }}</label>
    @endif
    <div class="input-group
            log-event" id="{{ $elementId }}" data-td-target-input="nearest"
        data-td-target-toggle="nearest">
        <span style="width: 100%" data-td-target="#{{ $elementId }}" data-td-toggle="datetimepicker">
            <input @if (isset($value)) value="{{ $value }}" @endif name="{{ $inputId }}"
                id="{{ $inputId }}" type="text" class="form-control" data-td-target="#{{ $elementId }}"
                @if (isset($isRequired) && $isRequired == 'true') autocomplete="off"
            required @endif>
        </span>
    </div>
</div>

@push('script')
    <script>
        new tempusDominus.TempusDominus(document.getElementById('{{ $elementId }}'), {
            useCurrent: false,
            localization: {
                hourCycle: 'h23',
                dateFormats: {
                    L: 'MM/dd/yyyy HH:mm:ss',
                },
                format: 'L',
                locale: 'id-ID'
            },
            display: {
                sideBySide: true,
                buttons: {
                    today: true,
                    clear: true,
                },
                components: {
                    seconds: true,
                }
            },

        });
    </script>
@endpush
