<br>
<div class="form-group">
    <div class="card">
        <div class="card-body">
            <h4>{{ $datas[0] }}</h4>
            @if (count($datas[1]['body']) > 0)
                <table @if (isset($datas[1]['class_table'])) class="{{ $datas[1]['class_table'] }}"  @endif>
        <thead>
            <tr>
                <th @if (isset($datas[1]['style_header'])) style="{{ $datas[1]['style_header'] }}"  @endif>
                    No
                    </th>
                    @foreach ($datas[1]['header'] as $headerKey => $headerItem)
                        <th @if (isset($datas[1]['style_header'])) style="{{ $datas[1]['style_header'] }}"  @endif>
                            {{ $headerKey }}
                        </th>
                    @endforeach
                    </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($datas[1]['body'] as $bodyItem)
                            <tr>
                                <td>{{ $i++ }}</td>
                                @foreach ($datas[1]['header'] as $headerKey => $headerItem)
                                    <td>{{ $bodyItem[$headerItem] }}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <table @if (isset($datas[1]['class_table'])) class="{{ $datas[1]['class_table'] }}"  @endif >
        <thead>
            <tr>
                <th @if (isset($datas[1]['style_header'])) style="{{ $datas[1]['style_header'] }}"  @endif>
                    No
                    </th>
                    @foreach ($datas[1]['header'] as $headerKey => $headerItem)
                        <th @if (isset($datas[1]['style_header'])) style="{{ $datas[1]['style_header'] }}" @endif>
                            {{ $headerKey }}
                        </th>
                    @endforeach
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="{{ count($datas[1]['header']) + 1 }}" class="text-center">
                                <h5>Data Empty</h5>
                            </td>
                        </tr>
                    </tbody>
                </table>

            @endif
        </div>
    </div>
</div>
