<div class="col-sm-12" id="htmlTarget">
  
    <div class="input-group log-event" id="{{ $elementId }}" data-td-target-input="nearest"
        data-td-target-toggle="nearest">
        
        <input style="width: 200px;" name="{{ $nameId }}" id="{{ $inputId }}" type="text" class="form-control form-control-sm"
            data-td-target="#{{ $elementId }}"
            value="{{$valueId}}"
            @isset($isRequired)
            required
            @endisset data-td-target="#{{ $elementId }}" data-td-toggle="datetimepicker">
        {{-- <span class="input-group-text" data-td-target="#{{ $elementId }}" data-td-toggle="datetimepicker">
            <i class="fas fa-calendar"></i>
        </span> --}}
    </div>
</div>

@push('script')
    <script>
        new tempusDominus.TempusDominus(document.getElementById('{{ $elementId }}'), {
            useCurrent: false,
            localization: {
                hourCycle: 'h23',
                dateFormats: {
                    L: 'yyyy-MM-dd HH:mm:ss',
                },
                format: 'L',
                locale: 'id-ID'
            },
            display: {
                sideBySide: true,
                buttons: {
                    today: true,
                    clear: true,
                },
                components: {
                    seconds: true,
                }
            },

        });
    </script>
@endpush
