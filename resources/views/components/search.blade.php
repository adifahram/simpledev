<div>
    @php
    if ($get) {
        // dd($get);
        $rowIndex = isset($get['param']) ? count($get['param']) : 0;
    } else {
        $rowIndex = 0;
    }
@endphp
    <div class="card">
        <div class="card-body">
            <form id="formInput">
                <div class="forms">
                    @if ($get && isset($get['param']))
                        @foreach ($get['param'] as $index => $param)
                            <div class="row mb-4">
                                <div class="col-3">
                                    <select name="param[]" id="param{{ $index }}" class="form-control" onchange="change({{ $index }})">
                                        @foreach ($columns as $key => $item)
                                            @foreach ($item as $fieldName => $field)
                                                <option value="{{ $fieldName }}" @if($param === $fieldName) selected @endif>{{ $field['Label'] }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-7">
                                    <input type="text" class="form-control" id="inputan{{ $index }}" name="inputan[]" placeholder="Search..." value="{{ $_GET['inputan'][$index] ?? '' }}">
                                </div>
                                @if ($index != 0)
                                <div class="col-2">
                                    <button onclick="removeRow(this)" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                </div>
                                @endif
                               
                            </div>
                        @endforeach
                    @else
                        <div class="row mb-4">
                        <div class="col-3">
                            <select name="param[]" id="param0" class="form-control" onchange="change(0)">
                                @foreach ($columns as $key => $item)
                                    @foreach ($item as $fieldName => $field)
                                        <option value="{{ $fieldName }}">{{ $field['Label'] }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="col-7">
                            <input type="text" class="form-control" id="inputan0" name="inputan[]" placeholder="Search...">
                        </div>
                    </div>
                    @endif
                    
                    
                </div>
                
                <button class="mb-4 btn btn-success"><i class="far fa-eye"></i> Search</button>
                <button type="button" onclick="tes()" class="mb-4 btn btn-warning"><i class="far fa-list-alt"></i> List All</button>
                <button onclick="add()" class="mb-4 btn btn-primary" type="button"><i class="far fa-plus-square "></i> Add Row</button>
            </div>
            </form>
    </div>
</div>

@push('script')
<script>
    var rowIndex = {{ $rowIndex ?? 0 }};

    function tes() {
        var baseUrl = window.location.href.split('?')[0];
        // Me-refresh halaman dengan URL dasar
        window.location.href = baseUrl;
    }
    function add() {
        rowIndex++;
        console.log('Row Index:', rowIndex);
        addNewSet(); // Call the function to add a new set of elements
    }

    function addNewSet() {
        // Clone the existing row and append it to the form
        var newRow = $('.row.mb-4:first').clone();
        // newRow.find('input, select').val(''); // Clear the values of the new row

        var templateInput = newRow.find('input');
        var templateSelect = newRow.find('select');
        
        // Assign IDs to the input and select elements
        var inputId = 'inputan' + rowIndex;
        templateInput.attr('id', inputId);



        var selectId = 'param' + rowIndex;
        templateSelect.attr('id', selectId);
        templateSelect.attr('onchange', 'change(' + rowIndex + ')'); // Update onchange attribute

        newRow.append(`
            <div class="col-2">
                <button onclick="removeRow(this)" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
            </div>`); // Add a remove button

        $('.forms').append(newRow);
        var inputElement = '<input type="text" class="form-control" name="inputan[]" id="inputan' + rowIndex + '" placeholder="Search...">';
        $('#inputan' + rowIndex).replaceWith(inputElement);
    }

    function removeRow(button) {
        $(button).closest('.row.mb-4').remove(); // Remove the parent row when the remove button is clicked
    }

    function change(index) {
        var val = $('#param' + index).val();
            console.log('Row Index:', index);
            console.log('Selected Value: ' + val);

            // Get the selected data from the columns array
            const data = @json($columns);
            console.log('data:', data);

            const selData = data.find(item => item[val]);
           

            console.log('selData: ' + selData);

            // Check if selData exists and log the "Label" property
            if (selData && Object.keys(selData).length > 0) {
                var keyOfSelData = Object.keys(selData)[0];
                var label = selData[keyOfSelData]['Label'];
                var type = selData[keyOfSelData]['type'];

                var inputElement = '<input type="'+type+'" class="form-control" name="inputan[]" id="inputan' + index + '" placeholder="' + label + '">';

                if (type === 'array' && selData[keyOfSelData]['values']) {
                    var values = selData[keyOfSelData]['values'];
                    var options = values.map(function (value) {
                        return '<option value="' + value['value'] + '">' + value['label'] + '</option>';
                    });
                    var selectElement = '<select class="form-control" name="inputan[]" id="inputan' + index + '">' + options.join('') + '</select>';
                    inputElement = selectElement;
                }

                // Replace the existing input with the dynamically generated element
                $('#inputan' + index).replaceWith(inputElement);
                $('#inputan' + index).attr('type', type);
            } else {
                console.log('Selected data not found for value: ' + val);
            }
        
    }
</script>
@endpush
