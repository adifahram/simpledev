<div>
    <div class="card">
        <div class="card-header">
            @if (isset($datas['create']))
                <div class="float-right">
                    <a href="{{ url('/') }}/{{ $datas['create'] }}" class="btn btn-info"><i
                            class="far fa-plus-square"></i> Add New</a>
                </div>
            @endif



        </div>
        <div class="card-body">
            <div class="table-responsive">


                <table class="table table-bordered">
                    <thead>
                        <tr>
                            {{-- <th class="checkbox-area" scope="col">
                                <input class="form-check-input" id="hover_parent_all" type="checkbox">

                            </th> --}}
                            <th scope="col" style="background-color: #0e1726; color:#FFFF"> No </th>
                            @foreach ($datas['fields'] as $key => $item)
                                <th scope="col" style="background-color: #0e1726; color:#FFFF">{{ $item[0] }}
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach ($datas['data'] as $keys => $item)
                            <tr>
                                <td>
                                    {{ $no++ }}
                                </td>
                                {{-- <td>
                                <div class="form-check form-check-primary">
                                    <input class="form-check-input hover_child" type="checkbox">
                                </div>
                            </td> --}}
                                @foreach ($datas['fields'] as $key => $itm)
                                    @if (is_array($itm[1]))
                                        @if ($itm[0] == 'Action')
                                            <td class="text-center">
                                                @foreach ($itm[1] as $ky => $idx)
                                                    @if ($ky == 'edit')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}/edit"
                                                            class="btn btn-sm btn-success"> <i
                                                                class="far fa-edit"></i></a>
                                                    @elseif($ky == 'delete')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}/delete"
                                                            class="btn btn-sm btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete this item?');"><i
                                                                class="far fa-trash-alt"></i></a>
                                                    @elseif(
                                                            ($ky == 'cancel_reason' && $item->status == 'BOOKED') ||
                                                            ($ky == 'cancel_reason' && $item->status == 'BOOKING PRAVERIFIED') ||
                                                            ($ky == 'cancel_reason' && $item->status == 'SERVICE ORDER OPEN') ||
                                                            ($ky == 'cancel_reason' && $item->status == 'WORK ORDER DRAFTED') ||
                                                            ($ky == 'cancel_reason' && $item->status_invoice == 'INVOICE CREATED')
                                                            )
                                                        <button class="btn btn-sm btn-danger"
                                                            onclick="cancel_reason('{{ url('/') }}/{{ $idx }}/{{ $item->id }}/cancel')">
                                                            <i class="far fa-trash-alt"></i></button>
                                                    @elseif($ky == 'show')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}"
                                                            class="btn btn-sm btn-info"><i class="far fa-eye"></i></a>
                                                    @elseif($ky == 'reject')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}/delete"
                                                            class="btn btn-sm btn-danger"
                                                            onclick="return confirm('Are you sure you want to reject this item?');"><i
                                                                class="far fa-times-circle"></i></a>
                                                    @elseif($ky == 'add')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}"
                                                            class="btn btn-sm btn-info"><i
                                                                class="far fa-plus-square"></i></a>
                                                    @elseif($ky == 'showIndex')
                                                        <a href="{{ url('/') }}/{{ $idx }}?reference_id={{ $item->id }}"
                                                            class="btn btn-sm btn-info"><i class="far fa-eye"></i></a>
                                                    @elseif($ky == 'pdf')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}/pdf"
                                                            class="btn btn-sm btn-danger" target="_blank"><i
                                                                class="far fa-file-pdf"></i></a>
                                                    @elseif($ky == 'excel')
                                                        <a href="{{ url('/') }}/{{ $idx }}/{{ $item->id }}/excel"
                                                            class="btn btn-sm btn-success"><i
                                                                class="far fa-file-excel"></i></a>
                                                    @elseif($ky == 'download')
                                                        <a href="{{ $item->url }}" target="_blank"
                                                            class="btn btn-sm btn-success"><i
                                                                class="far fa-file-excel"></i></a>
                                                    @endif
                                                @endforeach
                                            </td>
                                        @else
                                            @if ($item->$key == null)
                                                <td>-</td>
                                            @endif
                                            @foreach ($itm[1] as $ky => $idx)
                                                @if ($ky == $item->$key)
                                                    <td class="text-left">{{ $idx ? $idx : '-' }}</td>
                                                @endif
                                            @endforeach
                                        @endif
                                    @elseif ($itm[1] == 'string')
                                        <td class="text-left">{{ $item->$key ? $item->$key : '-' }}</td>
                                    @elseif ($itm[1] == 'image')
                                        <td class="text-left">
                                            @if ($item->$key)
                                                <img src="{{ url($item->$key) }}" alt="" width="150px">
                                            @endif
                                        </td>
                                    @elseif ($itm[1] == 'image_s3')
                                        <td class="text-left">
                                            @if ($item->$key)
                                                <img src="{{ $item->$key }}" alt="" width="150px">
                                            @endif
                                        </td>
                                    @elseif ($itm[1] == 'right')
                                        <td class="text-right">{{ $item->$key ? $item->$key : '-' }}</td>
                                    @elseif ($itm[1] == 'center')
                                        <td class="text-center">{{ $item->$key ? $item->$key : '-' }}</td>
                                    @elseif ($itm[1] == 'number')
                                        <td class="text-right">{{ number_format($item->$key) }}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <div class="d-flex ">
                    {!! $datas['data']->appends(request()->except('page'))->links() !!}
                </div>
            </div>
        </div>
    </div>

</div>

@push('script')
    <script>
        checkall('hover_parent_all', 'hover_child');

        function cancel_reason(url) {
            Swal.fire({
                title: "Please, Fill Reasons for Cancellation",
                input: "textarea",
                showCancelButton: true,
                confirmButtonText: "Submit",
                showLoaderOnConfirm: true,
                preConfirm: async (data) => {
                    const response = await fetch(url, {
                        method: 'POST',
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "credentials": "same-origin",
                            "X-CSRF-TOKEN": $('input[name="_token"]').val()
                        },
                        body: JSON.stringify({
                            remark: data,
                        })
                    }).then(res => {
                        if (!res.ok) {
                            return res.text().then(text => {
                                Swal.showValidationMessage(text);
                            })
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.isConfirmed) {
                    location.reload();
                }
            });
        }
    </script>
@endpush
