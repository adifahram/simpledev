@extends('layouts.apps')

@section('title')
{{$title}}
@endsection

@section('content')

    <x-search :columns="$columns" :get="$_GET"/>
    <br>
    <x-table :datas="$datas"/>

    
@endsection
