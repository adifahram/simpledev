@extends('layouts.apps')

@section('title')
{{$title}}
@endsection

@section('content')



<div class="card">
    <div class="card-body">
        <div class="row">
            <form action="{{ route('rolemenu.store') }}" method="post">
                @csrf
                <input type="hidden" name="id" id="" value="{{$id}}">
                <ul>
                    @foreach ($datas as $item)
                        <li>
                            @php
                                $selected = '';

                                foreach ($group->menus as $val) {
                                if ($val->id == $item->id) {
                                    $selected = 'checked';
                                    break;
                                }
                                }
                            @endphp
                            <input type="checkbox"
                           
                           
                            id="menu_{{ $item->id }}" {{$selected}} name="selected_menus[]" value="{{ $item->id }}">
                            {{ $item->name }}
                        </li>
                        @if ($item->details->count())
                            {!! printMenu($item->details,$group, 'menu_'.$item->id) !!}
                        @endif
                    @endforeach
                </ul>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection
@php
    function printMenu($items,$group) {
        $html = '<ul>';
        foreach ($items as $item) {
                $selected = '';
                foreach ($group->menus as $val) {
                    if ($val->id == $item->id) {
                        $selected = 'checked';
                        break;
                    }
                }
            $html .= ' <li> <input type="checkbox"
                    '. $selected.' id="menu_'.$item->id.'" name="selected_menus[]" value="'.$item->id.'">' . $item->name . '</li>';
            if ($item->details->count()) {
                $html .= printMenu($item->details,$group);
            }
        }
        $html .= '</ul>';

        return $html;
    }
@endphp