<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class FormInputDetail extends Component
{
    public $datas;
    public $id;
    public $details;

    public function __construct( $datas, $id, $details)
    {
        $this->datas = $datas;
        $this->id = $id;
        $this->details = $details;
    }
    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.form-input-detail');
    }
}
