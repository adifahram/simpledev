<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Search extends Component
{
   
    public $columns;
    public $get;

    public function __construct($columns,$get)
    {
        $this->columns = $columns;
        $this->get = $get;
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.search');
    }
}
