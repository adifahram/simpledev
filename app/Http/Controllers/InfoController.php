<?php

namespace App\Http\Controllers;

use App\Models\Info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InfoController extends Controller
{
    public function index() {
        return $this->create();
    }
    public function create() {
        // dd($datas);
        $info = Info::first();

        if ($info) {
            $data['datas']['datas'] = forms([
                'Id' => array('type' => 'hidden', 'col' => 12, 'required' =>true),
                'Name' => array('type' => 'text', 'col' => 12, 'required' =>true),
                'Email' => array('type' => 'text', 'col' => 12, 'required' =>true),
                'Photo' => array('type' => 'file', 'col' => 12),
                'Phone' => array('type' => 'text', 'col' => 12, 'required' =>true),
            ]);
            $data['datas']['data'] =  $info;
        } else {
            $data['datas']['datas'] = forms([
                'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
                'Email' => array('type' => 'text', 'col' => 3, 'required' =>true),
                'Photo' => array('type' => 'file', 'col' => 3, 'required' =>true),
                'Phone' => array('type' => 'text', 'col' => 3, 'required' =>true),
            ]);
        }

        $data['title'] = 'Menu Info';
        $data['datas']['route'] = 'info.store';
        $data['datas']['method'] = 'POST';

        return view('template.create', $data);
    }


    public function store(Request $request) {
        DB::beginTransaction();

        try {
            \Illuminate\Support\Facades\Log::info(json_encode($request->all()));
            if($request->file('photo')){

                $image = $request->file('photo');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('uploads/info'), $imageName);

                $img = 'uploads/info/'.  $imageName;

                $menu = Info::updateOrCreate([
                    'id' =>$request->id],
                    [
                    'name' =>$request->name,
                    'email' =>$request->email,
                    'phone' =>$request->phone,
                    'photo' =>$img,
                ]);
            }else {
                $menu = Info::updateOrCreate([
                    'id' =>$request->id],
                    [
                    'name' =>$request->name,
                    'email' =>$request->email,
                    'phone' =>$request->phone,
                ]);
            }


            session()->flash('message', 'Save Success.');

            DB::commit();
            return redirect('info/create');
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            // dd($e);

            session()->flash('error', 'Failed Save.');
           return redirect()->back();
        }


    }
}
