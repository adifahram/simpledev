<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    protected $models;
    public function __construct(Role $models)
    {
        $this->models = $models;
    }

    /**
     * Display a listing of the resource.
     */
    

    public function index(Request $request)
    {
        // dd(auth()->user()->menusa);
            $search = [];
            if ($request->all()) {
                $search = $request->all();
            }
       
            $create = 'role/create';
            $data = Role::filter($search)->latest()->paginate(10);
            $data = $this->list($data, $this->models, $create, 'Role Management');

            return view('template.index', $data);
    }

    public function create() {
        $datas = Role::latest()->pluck('name', 'id')->toArray();
        // dd($datas);
        $data['datas']['datas'] = fields([
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
        ]);

        $data['title'] = 'Role Management';
        $data['datas']['route'] = 'role.store';
        $data['datas']['method'] = 'POST';
       


        return view('template.create', $data);
    }

    public function store(Request $request) {
        DB::beginTransaction();

        try {
            \Illuminate\Support\Facades\Log::info(json_encode($request->all()));
           
            
            $role = Role::updateOrCreate([
                'id' =>$request->id],
                [
                'name'     => $request->name,
            ]);  
    
            session()->flash('message', 'Save Success.');

            DB::commit();
            return redirect('role'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            // dd($e); 

            session()->flash('error', 'Failed Save.');
           return redirect()->back();   
        }
    
        
    }


    public function edit($id) {

        $datas = Role::latest()->pluck('name', 'id')->toArray();
        $data['datas']['datas'] = fields([
            'Id' => array('type' => 'hidden', 'col' => 12),
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
        ]);


        $data['title'] = 'Role Management';
        $data['datas']['route'] = 'role.store';
        $data['datas']['method'] = 'POST';
        $data['datas']['data'] = Role::find($id);



        return view('template.create', $data);
    }

    public function show($id) {

        $data['datas'] = Menu::where('menu_id', NULL)->get();
        $data['group'] = $this->models->find($id);
        $data['title'] = 'Role';
        $data['id'] = $id;
       // dd($data['group']->menus);
        return view('role.group', $data);
    }

    public function destroy($id)  {
        DB::beginTransaction();

        try {

            $find = Role::find($id)->update(['deleted_by' => auth()->user()->id,'deleted_at' => date('Y-m-d H:i:s')]);
            session()->flash('message', 'Delete Success.');

            DB::commit();
            return redirect('role'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            session()->flash('error', 'Failed Save.');
           return redirect()->back();
        }
    }
}
