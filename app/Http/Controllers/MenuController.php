<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    protected $models;
    public function __construct(Menu $models)
    {
        $this->models = $models;
    }

    /**
     * Display a listing of the resource.
     */
    

    public function index(Request $request)
    {
            $search = [];
            if ($request->all()) {
                $search = $request->all();
            }
       
            $create = 'menu/create';
            $data = Menu::filter($search)->latest()->paginate(10);
            $data = $this->list($data, $this->models, $create, 'Menu Management');

            return view('template.index', $data);
    }

    public function create() {
        $datas =     Menu::where('menu_id', NULL)->get();
            
        $menuArray = [];

        foreach ($datas as $key => $value) {
            $menuArray[$value->id] = $value->name;
            $menuArray += $this->printMenu($value->details, 1);
        }
        // dd($datas);
        $data['datas']['datas'] = fields([
            'Menu' => array('type' => $menuArray, 'col' => 3, 'names' => 'menu_id'),
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
            'Route' => array('type' => 'text', 'col' => 3, 'required' =>true),
            'Is Active' => array('type' => array(
                "Y" => 'Aktif',
                "N" => 'Non Aktif',
            ), 'col' => 3, 'required' =>true),

        ]);

        $data['title'] = 'Menu Management';
        $data['datas']['route'] = 'menu.store';
        $data['datas']['method'] = 'POST';
       


        return view('template.create', $data);
    }

    public function store(Request $request) {
        DB::beginTransaction();

        try {
            \Illuminate\Support\Facades\Log::info(json_encode($request->all()));
            if (!$request->id) {
                if (($request->menu_id) != null) {
                    $count_header = Menu::find($request->menu_id);
    
                    $count = Menu::where('menu_id', $request->menu_id)->latest()->first();
                    $count = isset($count->count) ? $count->count+1 : $count_header->count+1;
                }else{
                    
                    $count = Menu::where('menu_id', NULL)->latest()->first();
                    $count = isset($count->count) ? $count->count+100 : 1;
                   
    
                }
                $menu = Menu::updateOrCreate([
                    'id' =>$request->id],
                    [
                    'name'     => $request->name,
                    'icon'     => $request->icon,
                    'class'     => $request->class,
                    'route'     => $request->route,
                    'menu_id'     => $request->menu_id,
                    'count'     => $count,
                ]);  
            }else{
                $menu = Menu::updateOrCreate([
                    'id' =>$request->id],
                    [
                    'name'     => $request->name,
                    'icon'     => $request->icon,
                    'class'     => $request->class,
                    'route'     => $request->route,
                    'menu_id'     => $request->menu_id,
                ]);  
            }
            
            
    
            session()->flash('message', 'Save Success.');

            DB::commit();
            return redirect('menu'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            // dd($e); 

            session()->flash('error', 'Failed Save.');
           return redirect()->back();   
        }
    
        
    }

    public function printMenu($items, $depth = 0) {
        $result = [];
    
        foreach ($items as $item) {
            $result[$item->id] = str_repeat('-', $depth) . $item->name;
    
            if ($item->details->count()) {
                $result += $this->printMenu($item->details, $depth + 1);
            }
        }
    
        return $result;
    }
    public function edit($id) {

        $datas = Menu::where('menu_id', NULL)->get();
            
        $menuArray = [];

        foreach ($datas as $key => $value) {
            $menuArray[$value->id] = $value->name;
            $menuArray += $this->printMenu($value->details, 1);
        }
        $data['datas']['datas'] = fields([
            'Id' => array('type' => 'hidden', 'col' => 12),
            'Menu' => array('type' => $datas, 'col' => 3, 'names' => 'menu_id'),
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
            'Route' => array('type' => 'text', 'col' => 3, 'required' =>true),
            'Icon' => array('type' => 'text', 'col' => 3, 'required' =>true),
            'Class' => array('type' => 'text', 'col' => 3),
           
            

        ]);


        $data['title'] = 'Menu Management';
        $data['datas']['route'] = 'menu.store';
        $data['datas']['method'] = 'POST';
        $data['datas']['data'] = Menu::find($id);



        return view('template.create', $data);
    }

    public function destroy($id)  {
        DB::beginTransaction();

        try {

            $find = Menu::find($id)->update(['deleted_by' => auth()->user()->id,'deleted_at' => date('Y-m-d H:i:s')]);
            session()->flash('message', 'Delete Success.');

            DB::commit();
            return redirect('menu'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            session()->flash('error', 'Failed Save.');
           return redirect()->back();
        }
    }
}
