<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $models;
    public function __construct(User $models)
    {
        $this->models = $models;
        // $this->middleware('auth:admin');
    }

    public function logout(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function index(Request $request) {
        
//  return (auth()->user()->menusa);
        $search = $request->all();
        $data = User::filter($search)->latest()->paginate(1);
        $data = $this->list($data, $this->models);
        // dd($data);
        return view('index', $data);
    }
}
