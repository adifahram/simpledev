<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function list($datas, $columns, $create = NULL, $message = 'success', $code = 200)
    {
        // return Response::json([
        //     'message' => $message,
        //     'code' => $code,
        //     'data' => $data,
        //     'fields' => $columns->fields(),
        //     'columns' => $columns->columns(),
        // ], $code);


        $data['datas']['create'] = $create;
        $data['datas']['data'] = $datas;
        $data['datas']['fields'] = $columns->fields();
        $data['columns'] = $columns->columns();
        $data['title'] = $message;

        return $data;
    }

}
