<?php

namespace App\Http\Controllers;

use App\Models\MenuRole;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuRoleController extends Controller
{
    protected $models;
    public function __construct(MenuRole $models)
    {
        $this->models = $models;
    }

    /**
     * Display a listing of the resource.
     */
    

    public function index(Request $request)
    {
            $search = [];
            if ($request->all()) {
                $search = $request->all();
            }
       
            $create = 'rolemenu/create';
            $data = MenuRole::filter($search)->latest()->paginate(10);
            $data = $this->list($data, $this->models, $create, 'Role Management');

            return view('template.index', $data);
    }

    public function create() {
        $datas = MenuRole::latest()->pluck('name', 'id')->toArray();
        // dd($datas);
        $data['datas']['datas'] = fields([
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
        ]);

        $data['title'] = 'Role Management';
        $data['datas']['route'] = 'role.store';
        $data['datas']['method'] = 'POST';
       


        return view('template.create', $data);
    }

    public function store(Request $request) {
        DB::beginTransaction();

        try {
            \Illuminate\Support\Facades\Log::info(json_encode($request->all()));
           
            $data = Role::find($request->id);
            
            $ids = [];
            foreach ($request->get('selected_menus') as $key => $value) {
                // dd($value);
                if ($value != NULL) {
                    $ids['menu'][$key]['menu_id'] = $value;
                    $ids['menu'][$key]['role_id'] = $request->id;
                }
              
            }
            // dd($ids);
            $data->menus()->detach();

            // Attach new relationships
            $data->menus()->sync($ids['menu']);
            // $data = MenuRole::insert($ids['menu']);
    
            session()->flash('message', 'Save Success.');

            DB::commit();
            return redirect('role'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            // dd($e); 

            session()->flash('error', 'Failed Save.');
           return redirect()->back();   
        }
    
        
    }


    public function edit($id) {

        $datas = MenuRole::latest()->pluck('name', 'id')->toArray();
        $data['datas']['datas'] = fields([
            'Id' => array('type' => 'hidden', 'col' => 12),
            'Name' => array('type' => 'text', 'col' => 3, 'required' =>true),
        ]);


        $data['title'] = 'Role Management';
        $data['datas']['route'] = 'role.store';
        $data['datas']['method'] = 'POST';
        $data['datas']['data'] = MenuRole::find($id);



        return view('template.create', $data);
    }

    public function destroy($id)  {
        DB::beginTransaction();

        try {

            $find = MenuRole::find($id)->update(['deleted_by' => auth()->user()->id,'deleted_at' => date('Y-m-d H:i:s')]);
            session()->flash('message', 'Delete Success.');

            DB::commit();
            return redirect('role'); 
        } catch (\Exception $e) {
            DB::rollBack();
            \Illuminate\Support\Facades\Log::error($e);
            session()->flash('error', 'Failed Save.');
           return redirect()->back();
        }
    }
}
