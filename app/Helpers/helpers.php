<?php

use Illuminate\Support\Str;


function formatPriceRp($str) {
    return 'Rp. ' . number_format($str, '0', '', '.');
}


 function columns($arr = [])
    {
        $datas = collect();
        foreach ($arr as $key => $val) {
            $snake = Str::snake($key);
            if (is_array($val)) {
                $values = collect();
                foreach ($val as $k => $v) {
                    $values->push([
                        'label' => $v,
                        'value' => $k,
                    ]);
                }
                $data = [$snake => [
                    'Label' => $key,
                    'type' => 'array',
                    'values' => $values->toArray(),
                ]];
                $datas->push($data);
            } else {
                $data = [$snake => [
                    'Label' => $key,
                    'type' => $val,
                ]];
                $datas->push($data);
            }
        }

        return $datas;
    }

    
 function fields($arr = [])
 {
     $datas = collect();
     foreach ($arr as $key => $val) {

         $snake = Str::snake($key);
       
        $datas[$snake] =  [$key,$val];

     }
    //  dd($datas);

     return $datas;
 }
 function forms($arr = [])
 {
     $datas = collect();
     foreach ($arr as $key => $val) {

         $snake = Str::snake($key);
       
        $datas[$snake] =  [$key,$val];

     }
    //  dd($datas);

     return $datas;
 }
