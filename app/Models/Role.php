<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends Model
{
    use HasFactory, SoftDeletes ;
    protected $guarded = [];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();
            $model->created_by = $user ? $user->id : null;
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user ? $user->id : null;
        });
    }


    public function scopeFilter($query, $request)
    {
        
        if (isset($request['param'])) {
            foreach ($request['param'] as $key => $val) {
                switch ($val) {
                    case null:
                        $query->orWhere($val, 'like', '%' . $request['inputan'][$key] . '%');
                        break;
                    default:
                        $query->orWhere($val, 'like', '%' . $request['inputan'][$key] . '%');
                        break;
                }
            }
        }
        
        // dd($query);
        return $query;
    }

    public static function columns()
    {
        return columns([
            'Name' => 'string',
        ]);
    }

    public static function fields()
    {
        return fields([
           
            'Name' => 'string',
            'Action' => array(
                "show" => 'role',
                "edit" => 'role',
                "delete"  => 'role'
            ),
           
        ]);
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_role')->orderBy('count');
    }

    
    public function getMenusaAttribute()
    {
        dd($this->menus);
        return $this->menus ? $this->menus->name : '-';
    }
}
