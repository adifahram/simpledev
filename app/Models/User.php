<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();
            $model->created_by = $user ? $user->id : null;
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user ? $user->id : null;
        });
    }


    public function scopeFilter($query, $request)
    {
        
        if (isset($request['param'])) {
            foreach ($request['param'] as $key => $val) {
                foreach ($this->columns() as $keys => $values) {
                    if ($key == $keys ) {
                        $columnName = array_key_first($values);
                        // dd($key);
                        switch ($columnName) {
                            case null:
                                $query->orWhere($columnName, 'like', '%' . $request['inputan'][$key] . '%');
                                break;
                            default:
                                $query->orWhere($columnName, 'like', '%' . $request['inputan'][$key] . '%');
                                break;
                        }
                    }
                }
                
            }
        }
        
        // dd($query);
        return $query;
    }

    public static function columns()
    {
        return columns([
            'Name' => 'string',
            'Url' => 'string',
            'Is Active' => array(
                "Y" => 'Aktif',
                "N" => 'Non Aktif',
            ),
        ]);
    }

    public static function fields()
    {
        return fields([
            // 'Is Active' => array(
            //     "Y" => 'Aktif',
            //     "N" => 'Non Aktif',
            // ),
            'Name' => 'string',
            // 'Url' => 'string',
            'Action' => array(
                "edit" => 'user/edit',
                "delete"  => 'user/delete'
            ),
           
        ]);
    }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function group()
    {
        return $this->belongsTo(Role::class, 'role', 'name');
    }

    public function getMenusaAttribute()
    {
        return $this->group ? $this->group->menus : '-';
    }
}
