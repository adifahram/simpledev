<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Menu extends Model
{
    use HasFactory, SoftDeletes ;
    protected $guarded = [];

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();
            $model->created_by = $user ? $user->id : null;
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user ? $user->id : null;
        });
    }

    public function details()
    {
        return $this->hasMany(Menu::class, 'menu_id', 'id');
    }

    public function scopeFilter($query, $request)
    {
        
        if (isset($request['param'])) {
            foreach ($request['param'] as $key => $val) {
                switch ($val) {
                    case null:
                        $query->orWhere($val, 'like', '%' . $request['inputan'][$key] . '%');
                        break;
                    default:
                        $query->orWhere($val, 'like', '%' . $request['inputan'][$key] . '%');
                        break;
                }
            }
        }
        
        // dd($query);
        return $query;
    }

    public static function columns()
    {
        return columns([
            'Name' => 'string',
            'Route' => 'string',
        ]);
    }

    public static function fields()
    {
        return fields([
           
            'Menu' => 'string',
            'Name' => 'string',
            'Route' => 'string',
            'Action' => array(
                "edit" => 'menu',
                "delete"  => 'menu'
            ),
           
        ]);
    }
    
    
    /**
     * Get the menus that owns the MMenu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menus()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }
    public function getMenuAttribute()
    {
        return $this->menus ? $this->menus->name : '-';
    }
}
