<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Info extends Model
{
    use HasFactory, SoftDeletes ;

    public static function boot()
     {
        parent::boot();
        static::creating(function($model)
        {
            $user = Auth::user();           
            $model->created_by = $user ? $user->id : NULL; 
        });
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user ? $user->id : NULL; 
        });       
    }
    protected $guarded = [];


    
    public function scopeFilter($query, $request)
    {
        
        if (isset($request['param'])) {
            foreach ($request['param'] as $key => $val) {
                foreach ($this->columns() as $keys => $values) {
                    if ($key == $keys ) {
                        $columnName = array_key_first($values);
                        // dd($key);
                        switch ($columnName) {
                            case null:
                                $query->orWhere($columnName, 'like', '%' . $request['inputan'][$key] . '%');
                                break;
                            default:
                                $query->orWhere($columnName, 'like', '%' . $request['inputan'][$key] . '%');
                                break;
                        }
                    }
                }
                
            }
        }
        
        // dd($query);
        return $query;
    }

    public static function columns()
    {
        return columns([
            'Name' => 'string',
            'Email' => 'string',
            'Phone' => 'string',
        ]);
    }

    public static function fields()
    {
        return fields([
            'Name' => 'string',
            'Email' => 'string',
            'Photo' => 'image',
            'Phone' => 'string',
            'Action' => array(
                "edit" => 'menu',
                "delete"  => 'menu'
            ),
           
        ]);
    }
}
